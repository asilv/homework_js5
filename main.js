let newUser = generateUser();

function generateUser() {
    return {
        name: prompt("Enter you`re name"),
        lastName: prompt("Enter you`re last name"),
        birthday: prompt("Enter enter you`re Birthday mm.dd.yyyy"),
        getLogin() {
            return (this.name[0] + this.lastName).toLowerCase();
        },
        getAge() {
            const year = this.birthday.split('.')[2];
            const month = this.birthday.split('.')[1];
            const day = this.birthday.split('.')[0];
            return Math.floor((new Date().getTime() - new Date(year, month - 1, day)) / (24 * 3600 * 365.25 * 1000)) || 0;
        },
        getPassword() {
            return this.name[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(-4);
        },
    }
}

console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());
